package id.ac.ui.cs.advprog.tutorial0.service;

import id.ac.ui.cs.advprog.tutorial0.model.Activity;
import id.ac.ui.cs.advprog.tutorial0.model.Day;

import java.util.List;

public interface ActivityService {
    Activity create(Activity activity);

    List<Activity> findAll();

    List<Activity> findByDay(Day day);
}
